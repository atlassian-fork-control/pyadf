from setuptools import setup

setup(
  name = 'pyadf',
  packages = ['pyadf', 'pyadf.inline_nodes', 'pyadf.inline_nodes.marks'],
  version = '0.2.29',
  description = 'Python Atlassian Document Format Library',
  author = 'Martin Doms',
  author_email = 'iamaelephant@gmail.com',
  url = 'https://bitbucket.org/mdoms/pyadf',
  keywords = ['atlassian', 'markdown', 'adf'],
  classifiers = [],
  license = 'MIT'
)
