FROM atlassian/default-image:1

RUN apt-get update && apt-get install -y git python3 python3-pip python-setuptools

RUN python3 -m pip install setuptools wheel twine